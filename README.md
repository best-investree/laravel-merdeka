# BEST PROJECT

<img src="public/homepage/image/homepage.png" height="200">

## Description
Best Project is a web-based application that is useful for providing information about the BEST program from Investree. The features include displaying information about what BEST is, the conditions for becoming a BEST participant, positions on BEST, and so on. Future features will be developed as needed.

## How To Install
1. Clone the github repo:

    ```bash
    git clone https://gitlab.com/best-investree/laravel-merdeka.git
    ```
2. Go the project directory:

    ```bash
    cd laravel-merdeka
    ```
3. Install the project dependencies:
    ```bash
    composer install
    ```
4. Don't forget to copy .env.example to root folder and rename it as .env after that config like this:
   ```bash
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=best-project
    DB_USERNAME=root
    DB_PASSWORD=
   ```
5. Run your XAMPP or MySQL and create an empty Database named best-project
6. Create tables into database using Laravel migration:
    ```bash
    php artisan migrate
    ```
7. Seed your database with this command:
    ```bash
    php artisan db:seed
    ```
8. Setup your package.json
    ```bash
    {
        "private": true,
        "scripts": {
            "test:unit": "vue-cli-service test:unit",
            "dev": "npm run development",
            "development": "mix",
            "hot": "mix watch --hot",
            "prod": "npm run production",
            "production": "mix --production",
            "watch": "mix watch",
            "watch-poll": "mix watch -- --watch-options-poll=1000"
        },
        "devDependencies": {
            "@vue/cli-plugin-unit-jest": "^4.5.17",
            "@vue/test-utils": "^1.3.0",
            "axios": "^0.25",
            "bootstrap": "^5.2.1",
            "eslint": "^8.12.0",
            "eslint-plugin-vue": "^8.5.0",
            "laravel-mix": "^6.0.6",
            "laravel-vite-plugin": "^0.6.0",
            "lodash": "^4.17.19",
            "postcss": "^8.1.14",
            "sass": "^1.49.9",
            "sass-loader": "^12.6.0",
            "vite": "^3.0.0",
            "vue": "^2.7.10",
            "vue-fragment": "^1.6.0",
            "vue-loader": "^15.10.0",
            "vue-router": "^3.5.3",
            "vue-template-compiler": "^2.7.10"
        },
        "dependencies": {
            "@vitejs/plugin-vue2": "^2.0.0",
            "@vue/cli-service": "^4.5.17",
            "vuex": "3.1.1"
        }
    }
    ```
9. Install npm dependencies
    ```bash
    npm install
    ```
10. Start the laravel server:
    ```bash
    php artisan serve
    ```
11. Run watch:
    ```bash
    npm run watch
    ```
12. Lastly, open your browser and type localhost:8000, and enjoy the feature
## License

This project is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).


# Framework Contribution

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel is accessible, powerful, and provides tools required for large, robust applications.
