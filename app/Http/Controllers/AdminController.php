<?php

namespace App\Http\Controllers;

use App\Models\Register;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function index(){
        $data = DB::table('registers')
                ->join('users','registers.account_id', '=', 'users.id')
                ->select('users.id','users.name','users.email','registers.birth_date', 'registers.education', 'registers.major', 'registers.role')
                ->get();
        $no = 0;
        return view('admin.index',[
            'data' => $data,
            'no' => $no,
            'subtitle' => 'Admin Dashboard'
        ]);
    }  

    public function index2(){
        $data = User::with('register:register_id,account_id,birth_date,role,education,major')
        ->select('id','name','email')
        ->get();
        $no = 0;
        return view('admin.components.orm',[
            'data' => $data,
            'no' => $no,
            'subtitle' => 'Admin Dashboard'
        ]);
    }  

    public function index3(){
        $data = DB::select( DB::raw("SELECT users.id, users.name, users.email, registers.birth_date, registers.education, registers.major, registers.role FROM registers INNER JOIN users ON users.id = registers.account_id"));
        $no = 0;
        return view('admin.components.raw',[
            'data' => $data,
            'no' => $no,
            'subtitle' => 'Admin Dashboard'
        ]);
    }

    public function editregister($id)
    {
        $data = DB::table('registers')
                ->join('users','registers.account_id', '=', 'users.id')
                ->select('users.id','users.name','users.email','registers.birth_date', 'registers.education', 'registers.major', 'registers.role')
                ->where('registers.account_id','=', $id)
                ->first();
        return view('admin.components.edit', [
            'data'=> $data,
            'subtitle' => 'Edit Data Register'
        ]);
    }

    public function updateregister(Request $request, $id)
    {
        $account = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
        ]);

        $acc = User::where('id', $id);

        $acc->update([
            'name' => $request->name,
            'email' => $request->email
        ]);

        $register = $request->validate([
            'birth_date' => 'required',
            'education' => 'required',
            'major' => 'required',
            'role'=>'required'
        ]);

        $reg = Register::where('account_id', $id);

        $reg->update([
            'birth_date' => $request->birth_date,
            'education' => $request->education,
            'major' => $request->major,
            'role' => $request->role
        ]);

        if( $reg){
            return redirect('/admin')->with([
                'alert' => 'edit successfully!',
                'class' => 'alert-success'
            ]);
        }else{
            return redirect('/admin')->with([
                'alert' => 'edit failed!',
                'class' => 'alert-danger'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyregister($id)
    {
        $acc = User::find($id);
        $reg = Register::where('account_id', $id);
        $acc->delete();
        $reg->delete();
        return redirect('/admin')->with([
            'alert' => 'delete successfully!',
            'class' => 'alert-success'
        ]);
    }
}
