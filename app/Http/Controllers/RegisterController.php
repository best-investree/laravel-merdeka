<?php

namespace App\Http\Controllers;

use App\Models\Register;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function create(Request $request){
        $account = $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:8',
            'password_confirmation' => 'required'
        ]);

        $account['name'] = $account['firstname']." ".$account['lastname'];
        $account['password'] = Hash::make($request->password);

        $create_acc = User::create($account);

        $account_id = $create_acc->id;

        $register = $request->validate([
            'birth_date' => 'required',
            'education' => 'required',
            'major' => 'required',
            'role'=>'required'
        ]);

        $register['account_id'] = $account_id;

        $create_reg = Register::create($register);

        if($create_reg){
            return redirect('/')->with([
                'alert' => 'register successfully!',
                'class' => 'alert-success'
            ]);
        }else{
            return redirect('/')->with([
                'alert' => 'register failed!',
                'class' => 'alert-danger'
            ]);
        }
    }

    public function update(Request $request, $id){

    }

    public function destroy($id){

    }

}
