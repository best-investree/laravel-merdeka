<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    protected $table = 'registers';
    protected $primaryKey = 'register_id';
    protected $fillable = [
        'account_id',
        'birth_date',
        'education',
        'major',
        'role'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
