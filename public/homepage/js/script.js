// FOR HANDLING BUTTON UP
var scrollToTopBtn = document.querySelector(".scrollup");
var rootElement = document.documentElement;

function handleScroll() {
    var scrollTotal = rootElement.scrollHeight - rootElement.clientHeight
    if ((rootElement.scrollTop / scrollTotal) > 0.10) {
        scrollToTopBtn.classList.add("showBtn")
    } else {
        scrollToTopBtn.classList.remove("showBtn")
    }
}
document.addEventListener("scroll", handleScroll);


// interval alert
$(document).ready(function () {
    setInterval(() => {
        $('#alert-notif').hide();
    }, 2000);
})
