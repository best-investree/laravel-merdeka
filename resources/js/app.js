import './bootstrap';
import Vue from 'vue'
import VueRouter from 'vue-router'

import router from './router/index'

import HomeApp from './src/pages/home/HomeApp.vue'
import AdminApp from './src/pages/admin/AdminApp.vue'

Vue.use(VueRouter);

const app = new Vue({
    el: '#app',
    router,
    components: {
        HomeApp,
        AdminApp
    }
});
