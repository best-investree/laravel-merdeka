@extends('admin.layouts.app')

@section('content')
<div class="container-fluid">
    @foreach ($errors->all() as $error)
    <p style="color: red;">{{ $error }}</p>
    @endforeach
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header border-0">
                    <h3 class="card-title">Edit Data Register</h3>
                </div>
                <div class="card-body p-3">
                    <form action="{{ route('admin.update.register', $data->id) }}" method="POST">
                        @csrf
                        <div class="mb-3">
                            <label for="name" class="form-label">Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Your name"
                                value="{{ $data->name }}" required>
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Your email"
                                value="{{ $data->email }}" required>
                        </div>
                        <div class="mb-3">
                            <label for="birth_date" class="form-label">Date Of Birth</label>
                            <input type="date" class="form-control" id="birth_date" name="birth_date"
                                placeholder="Birth Date" value="{{ $data->birth_date }}" required>
                        </div>
                        <div class="mb-3">
                            <label for="education" class="form-label">Education</label>
                            <select class="form-control" name="education" id="education" required>
                                <option value="associate" {{ $data->education == 'associate' ? 'selected' : '' }}>
                                    Associate Degree
                                </option>
                                <option value="bachelor" {{ $data->education == 'bachelor' ? 'selected' : '' }}>
                                    Bachelor's Degree</option>
                                <option value="master" {{ $data->education == 'master' ? 'selected' : '' }}>Master's
                                    Degree</option>
                                <option value="doctoral" {{ $data->education == 'doctoral' ? 'selected' : '' }}>
                                    Doctoral's Degree</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="major" class="form-label">Major</label>
                            <select class="form-control" name="major" id="major" required>
                                <option value="TI" {{ $data->major == 'TI' ? 'selected' : '' }}>Informatic
                                    Engineering</option>
                                <option value="SI" {{ $data->major == 'SI' ? 'selected' : '' }}>Information System
                                </option>
                                <option value="SK" {{ $data->major == 'SK' ? 'selected' : '' }}>Computer System</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="role" class="form-label">Role</label>
                            <select class="form-control" name="role" id="role" required>
                                <option value="FE" {{ $data->role == 'FE' ? 'selected' : '' }}>Fullstack Engineer
                                </option>
                                <option value="QA" {{ $data->role == 'QA' ? 'selected' : '' }}>Quality Assurance
                                </option>
                                <option value="PO" {{ $data->role == 'PO' ? 'selected' : '' }}>Product Owner</option>
                                <option value="PM" {{ $data->role == 'PM' ? 'selected' : '' }}>Product Manager</option>
                                <option value="DE" {{ $data->role == 'DE' ? 'selected' : '' }}>Data Engineer</option>
                                <option value="DS" {{ $data->role == 'DS' ? 'selected' : '' }}>Data Scientist</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <button type="submit" class="btn btn-success">Edit Data</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
@endsection
