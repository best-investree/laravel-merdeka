<!DOCTYPE html>
<html lang="en">

@include('admin.layouts.header')

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <div id="app">
            <admin-app></admin-app>
        </div>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            @include('admin.layouts.breadcrumb')
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                @yield('content')
            </div>
            <!-- /.content -->

        </div>
        <!-- /.content-wrapper -->
        @include('admin.layouts.footer')
    </div>
    <!-- ./wrapper -->

</body>

</html>
