<!-- Main Footer -->
<footer class="main-footer">
    <strong>Copyright &copy; 2022</strong> - All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.0.0
    </div>
</footer>

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
@stack('datatable-script')
<!-- AdminLTE -->
<script src="{{ asset('vendor/adminlte/dist/js/adminlte.js') }}"></script>
{{-- Custom --}}
<script src="{{ asset('dash-admin/js/script.js') }}"></script>
<script src="{{ mix('js/app.js') }}"></script>
