{{-- LEFT SIDEBAR --}}
<aside class="main-sidebar elevation-4">
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 mb-3 pb-3 mx-auto d-flex">
            <div class="image">
                <img src="{{ asset('homepage/image/logo-best.png') }}" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Admin BEST</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            Data Register
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">3</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.index') }}" class="nav-link">
                                <i class="nav-icon fas fa-table"></i>
                                <p> Data Register (QB)</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.index.orm') }}" class="nav-link">
                                <i class="nav-icon fas fa-table"></i>
                                <p> Data Register (ORM)</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.index.raw') }}" class="nav-link">
                                <i class="nav-icon fas fa-table"></i>
                                <p> Data Register (Raw)</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>


{{-- RIGHT SIDEBAR --}}
<aside class="control-sidebar">

</aside>
