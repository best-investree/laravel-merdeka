<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- Title and Icon --}}
    <title>BEST - Home Page</title>
    <link rel="icon" href="{{ asset('homepage/image/logo-best.png') }}">

    {{-- Prerequisite Script --}}
    <script src="{{ asset('homepage/js/check.js') }}" defer></script>

    {{-- Stylesheet --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('homepage/css/styles.css') }}" rel="stylesheet">
</head>

<body id="top">
    @if (session()->has('alert'))
    <div class="alert{{ " ".session('class')." " }}alert-dismissible fade show mt-4" role="alert" id="alert-notif">
        {{ session('alert') }}
    </div>
    @endif

    <div class="scrollup">
        <a class="arrow up" href="#top"></a>
    </div>

    <div id="app">
        <home-app></home-app>
    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js">
    </script>
    <script src="{{ asset('homepage/js/script.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>
</body>

</html>
