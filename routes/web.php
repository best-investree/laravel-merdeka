<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\RegisterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomepageController::class, 'index'])->name('index');

Route::prefix('register')->group(function () {
    Route::post('/create',[RegisterController::class, 'create'])->name('register.create');
});



Route::prefix('admin')->group(function () {
    Route::get('/',[AdminController::class, 'index'])->name('admin.index');
    Route::get('/orm',[AdminController::class, 'index2'])->name('admin.index.orm');
    Route::get('/raw',[AdminController::class, 'index3'])->name('admin.index.raw');
    Route::get('/editregister/{id}',[AdminController::class, 'editregister'])->name('admin.edit.register');
    Route::post('/updateregister/{id}',[AdminController::class, 'updateregister'])->name('admin.update.register');
    Route::delete('/deleteregister/{id}',[AdminController::class, 'destroyregister'])->name('admin.destroy.register');
});